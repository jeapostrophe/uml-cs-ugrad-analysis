all: analysis.zip

analysis.zip: all.txt data.csv report.csv
	zip -9 $@ $^

TARGET = "Cornelius Sheehan"
report.csv: report.rkt data.csv index.csv
	racket -t report.rkt -- "^(COMP|91 )" "Computing I$$" index.csv ${TARGET} < data.csv > $@
	xsv search -s "Name" ${TARGET} data.csv
	wc -l $@

all.txt data.csv: all.pdf
	racket -t main.rkt
	(head -n 1 data.csv && tail -n +2 data.csv | sort) > data-sorted.csv
	mv data-sorted.csv data.csv
